/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useEffect, useRef, useState } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Alert,
  ActivityIndicator,
} from 'react-native';
import { Button } from 'react-native-elements'
import MaterialIcon from 'react-native-vector-icons/MaterialIcons'

import { DrawerActions, NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { WebView } from 'react-native-webview'

function LoadingView() {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <ActivityIndicator size="large" color="blue" />
    </View>
  )
}

const Drawer = createDrawerNavigator()

function DetailsScreen() {
  return (
    <Drawer.Navigator initialRouteName="MainDetails">
      <Drawer.Screen name="MainDetails" component={MainDetails} />
      <Drawer.Screen name="AnotherDetails" component={AnotherDetailsScreen} />
      <Drawer.Screen name="WebView" component={WebViewScreen} />
    </Drawer.Navigator>
  );
}

function AnotherDetailsScreen() {
  const [isLoading, setIsLoading] = useState(true)

  useEffect(() => {
    setTimeout(() => setIsLoading(false), 2500)
  }, [])

  return isLoading ? <LoadingView /> : (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Second details screen</Text>
    </View>
  )
}

function WebViewScreen() {
  const webview = useRef(null)

  const onMessageHandler = (data) => {
    Alert.alert("Received message", data)
  }

  return (
    <WebView
      ref={webview}
      startInLoadingState
      style={{ flex: 1 }}
      source={{ uri: "https://google.com" }}
    />
  )
}

function MainDetails({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Main details screen</Text>
    </View>
  )
}

const Stack = createStackNavigator();

function HomeScreen({ navigation }) {
  return (
    <>
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Button onPress={() => navigation.navigate("Details")} title="Navigate" />
      </View>
    </>
  );
};

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen
          name="Details"
          component={DetailsScreen}
          options={({ navigation }) => ({
            title: "Details",
            headerRight: () => (
              <Button
                onPress={() => navigation.dispatch(DrawerActions.toggleDrawer())}
                title="Toggle Drawer"
                containerStyle={{ backgroundColor: 'transparent', marginRight: 15 }}
                buttonStyle={{ backgroundColor: 'transparent' }}
                titleStyle={{ color: 'blue'}}
              />
            )
          })}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
